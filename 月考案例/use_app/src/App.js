import React ,{useState,useEffect} from 'react'
import {my_useState,my_useEffect} from "./index"

function App() {
  const [str,setStr] = useState("我是useSatte")
  const [my_str,setMy_str] = my_useState("我是my_useState")
  my_useEffect(()=>{
    console.log("my_useEffect");
  },[my_str])
  useEffect(()=>{
    console.log(111);
  },[my_str])
  const btn = () => {
    setStr("更改useState")
    setMy_str("更改my_useState")
  }
  return (
    <div>
      app
      {
        str
      }
      <br/>
      {
        my_str
      }
      <button onClick={btn}>点我</button>
    </div>
  )
}

export default App
