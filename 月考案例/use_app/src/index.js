import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

let stateDate = [] // 用来存储函数组件中的数据（缓存）
let Statei = 0 // 利用数组来保存数据

const root = ReactDOM.createRoot(document.getElementById('root'));
export const my_useState = (props) =>{ // 默认执行 第一次传参
  let createIndex = Statei
  // 判断stateDate 数组上一次位置有咩有数据 如果没有 说明没有传惨

  stateDate[createIndex] = stateDate[createIndex]?stateDate[createIndex]:props
  function setProps(newProps){ //newProps 是通过调用useState 第二个参数传入的函数
    //改变数组中当前位置的值 stateDate[createIndex] 是上一次调用的值
    stateDate[createIndex] = newProps// 赋值
    Statei = 0 // 更改初使值
    // 更改后重新渲染dom
    root.render(
        <App />
    );
  }
  Statei++ //每一次修改 步长都要改变
  return [stateDate[createIndex],setProps] // 返回数组让用户调用
}

export const my_useEffect = (callback,arr) =>{
  let oldState = stateDate[Statei]
  // 如果上一次调用有值 那么进行判断 数组去重后的长度和要监听的数据的长度 保持一致
  let isChange = oldState? [...new Set([...oldState,...arr])].length !== arr.length:false
  // debugger
  if (arr || isChange) {
    callback()
  }
  // 如果下次调用函数 缓存数组下标 ++
  stateDate[Statei++] =arr
}
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

