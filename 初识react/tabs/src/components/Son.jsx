import React from 'react'
import PropTypes from 'prop-types'
function Son({childrenData}) {
  return (
    <div className='son'> 
        {
            childrenData?.map((item,index)=>{
                return <div key={index} className="content">
                    <p style={{color:"red",fontSize:"20px"}}>name:{item.name}</p>
                    <p>age:{item.age}</p>
                    <img src={item.img} alt="图片错误" />
                </div>
            })
        }
    </div>
  )
}
// 默认参数
Son.defaultProps = {
    childrenData: [{
        name:"没有数据",
        age:0
    }]
}
// 类型校验
Son.propTypes = {
    childrenData: PropTypes.array.isRequired
}
export default Son