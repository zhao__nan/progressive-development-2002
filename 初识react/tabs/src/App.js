import React ,{useState,useEffect} from 'react'
import instance from "./utils/Request"
import { Tabs } from 'antd';
import Son from './components/Son';
const { TabPane } = Tabs;
function App() {
  const [data,setData] = useState([])
  const [childrenData,setChildrenData] = useState([])
  // 请求数据
  useEffect(()=>{
    instance.get("/list").then(res=>{
      if(res.data.code === "200"){
        setData(res.data.list)
        setChildrenData(res.data.list[0].children)
      }
    })
  },[])
  console.log(data);

  const onChange = (key) => {
    console.log(key);
    setChildrenData(data[key].children)
  };

  let my_son = {
    childrenData
  }
  return (
    <div className='app'>
       <Tabs defaultActiveKey="1" onChange={onChange}>
        {
          data&&data.map((item,index)=>{
            return  <TabPane tab={item.title} key={index}>
            <Son {...my_son}></Son>
          </TabPane>
          })
        }
      </Tabs>
    </div>
  )
}

export default App