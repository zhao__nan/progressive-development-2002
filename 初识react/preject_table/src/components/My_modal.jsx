import React ,{useEffect} from 'react'
import {Modal,Form,Input,Select,Button,DatePicker} from "antd"
import moment from 'moment';
function My_modal({flag,handleOk,handleCancel,data,edit_value}) {
    //创建一个表单实利
    const [form] = Form.useForm()

    useEffect(()=>{
        // console.log(form);
        // 监听edit_value发生的改遍 并判断对象里是否有key 如果有说明点击的是编辑按钮 如果没有说明点击的是新增按钮
        if (edit_value.key) {
            form.setFieldsValue({
                name:edit_value.name,
                age:edit_value.age,
                city:edit_value.city,
                state:edit_value.state,
            })
        }else{
            form.setFieldsValue({
                name:"",
                age:"",
                city:"",
                state:""
            })
        }
    },[edit_value])
    const onFinish = (values) => {
        values.title = edit_value.key?"编辑":"新增"
        values.key = data.length+1
        if (edit_value.key) {
            values.title = "编辑"
            values.id = edit_value.id
        }else{
            values.title = "新增"
            values.id = new Date().getTime()
        }  
        values.date = moment(values.date).format("YYYY-MM-DD")
        console.log('Success:', values);
        handleOk(values)
    };
    
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

  return (
    <div>
        <Modal destroyOnClose={true} forceRender={true} footer={null} title={edit_value.id?"编辑 modal" :"新增 modal"} visible={flag} onOk={handleOk} onCancel={handleCancel}>
        <Form
            form={form}
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 16,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            >

            <Form.Item
                label="Name"
                name="name"
                rules={[
                {
                    required: true,
                    message: 'Please input your username!',
                },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Age"
                name="age"
                rules={[
                {
                    required: true,
                    message: 'Please input your age!',
                },
                ]}
            >
                <Input />
            </Form.Item>
            
            <Form.Item
                label="City"
                name="city"
                rules={[
                {
                    required: true,
                    message: 'Please input your city!',
                },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="State"
                name="state"
                rules={[
                {
                    required: true,
                    message: 'Please input your state!',
                },
                ]}
            >
               <Select>
                    <Select.Option value="异常">异常</Select.Option>
                    <Select.Option value="关闭">关闭</Select.Option>
                    <Select.Option value="进行中">进行中</Select.Option>
                    <Select.Option value="已上线">已上线</Select.Option>
                </Select>
            </Form.Item>
           
            <Form.Item
                label="Date"
                name="date"
                rules={[
                {
                    required: true,
                    message: 'Please input your date!',
                },
                ]}
            >
                 <DatePicker defaultValue={edit_value.date} format={'YYYY-MM-DD'}/>
            </Form.Item>
           
            <Form.Item
                wrapperCol={{
                offset: 8,
                span: 16,
                }}
            >
                <Button type="primary" onClick={handleCancel}>
                     取消
                </Button>
                <Button type="primary" htmlType="submit">
                     Submit
                </Button>
            </Form.Item>
            </Form>
        </Modal>
    </div>
  )
}

export default My_modal