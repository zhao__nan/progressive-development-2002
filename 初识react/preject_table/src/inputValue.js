import React ,{useState} from 'react'

function App() {
  const [value,setValue] = useState("")
  return (
    <div>
      App
      <input type="text" value={value} onChange={(e) => setValue(e.target.value)}/>
    </div>
  )
}

export default App