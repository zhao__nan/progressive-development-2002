import React ,{useState,useEffect} from 'react'
import {Input,Button,Select,Space, Table ,message} from "antd"
import request from "./utils/Request"
import My_modal from './components/My_modal'

const {Option} = Select

function App() {
    const [data,setData] = useState([])
    const [copyData,setCopyData] = useState([])
    const [flag, setFlag] = useState(false);
    const [edit_value,setEdit_value] = useState({})
    const [search_value,setSearch_value] = useState("")
    const columns = [
        {
            title: 'Key',
            dataIndex: 'key',
            key: 'key',
        },
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
          render: (text) => <a>{text}</a>,
        },
        {
          title: 'Age',
          dataIndex: 'age',
          key: 'age',
        },
        {
          title: 'City',
          dataIndex: 'city',
          key: 'city',
        },
        {
            title: '状态',
            dataIndex: 'state',
            key: 'state',
          },
        {
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
        },
        {
          title: 'Action',
          key: 'action',
          render: (_, record) => (
            <Space size="middle">
              <Button onClick={()=>edit_data(record)}>edit</Button>
              <Button onClick={()=>del_data(record)}>Delete</Button>
            </Space>
          ),
        },
    ];
    //请求数据
    useEffect(()=>{
        request.get("/table").then(res=>{
            // console.log(res);
            if (res.status === 200) {
                setData(res.data.list)
                setCopyData(res.data.list)
            }
        })
    },[])

    //  筛选数据
    const set_state = (value) => {
        setData(copyData.filter(item => item.state  === value))
    }

    //重置按钮
    const get_data = () => {
        request.get("/table").then(res=>{
            if (res.status === 200) {
                setData(res.data.list)
                setCopyData(res.data.list)
            }
        })
    }

    // 删除函数
    const del_data = (record) =>{
        console.log(record);
        // 拷贝一份我们的总数据
        let newData = [...data]
        newData.forEach((item,index)=>{
            if (item.id === record.id) {
                newData.splice(index,1)
            }
        })
        setData(newData)
        setCopyData(newData)
        message.success("删除成功")
    }
    //搜索数据
    const search_data = () => {
        console.log(search_value);
        setData(copyData.filter(item=>{
            return item.name.includes(search_value) || item.city.includes(search_value) || item.state.includes(search_value)|| item.age.toString().includes(search_value)
        })) 
        setSearch_value("")
    }
    
    //编辑函数
    const edit_data = (record) => {
        setEdit_value(record)
        console.log(record);
        setFlag(true);
    }

    // 点击展示弹框
    const showModal = () => {
        setEdit_value({})
        setFlag(true);
    };

    // 弹框确认函数
    const handleOk = (values) => {
        console.log(",222",values);
        let newData = [...data]
        if (values.title === "新增") {
            newData.push(values)
            message.success("添加成功")
        }else{
           console.log("编辑",values);
            newData.forEach((item,index)=>{
                if (item.id === values.id) {
                    newData[index] = {...values}
                    message.success("编辑成功")
                }
            })
        }
        setData(newData)
        setCopyData(newData)
        setFlag(false);
    };

    // 弹框取消函数
    const handleCancel = () => {
        setFlag(false);
    };

    let my_modal = {
        flag,
        handleOk,
        handleCancel,
        data,
        edit_value
    }
    return (
        <div className='app'>
            <header>
                <div className='input'>
                    <p>规则名称： <Input value={search_value} style={{width:"200px"}}  placeholder="请输入" onChange={(e)=>setSearch_value(e.target.value)} /></p>
                    <div>使用状态： 
                        <Select style={{width: '200px',}} defaultValue="请选择" onChange={set_state}>
                            <Option value="异常">异常</Option>
                            <Option value="关闭">关闭</Option>
                            <Option value="进行中">进行中</Option>
                            <Option value="已上线">已上线</Option>
                        </Select>
                    </div>
                </div>
                <div className='btn'>
                    <Button type="primary" onClick={search_data}>查询</Button>
                    <Button onClick={get_data}>重置</Button>
                    <Button onClick={showModal}>新建</Button>
                </div>
            </header>
            <main>
                {
                    data&&<Table pagination={{pageSize:5}} columns={columns} dataSource={data} />
                }
                
            </main>
            <My_modal {...my_modal}></My_modal>
        </div>
    )
}

export default App