import React, { Component } from 'react'
import my_context from '../utils/my_context'
export class Index2 extends Component {
  render() {
    return (
      <div>
        <my_context.Consumer>
          {
            (value)=>{
              console.log(value);
              return <p>{value.name}</p>
            }
          }
        </my_context.Consumer>
      </div>
    )
  }
}

export default Index2
