import React ,{useRef,useEffect}from 'react'
import * as echarts from "echarts"
import * as my_options from "../utils/my_options"
function Ecahrts() {
    const chartRef = useRef()
    const chartRef1 = useRef()

    useEffect(()=>{
        // 创建一个echarts实例，返回echarts实例。不能在单个容器中创建多个echarts实例
        const chart = echarts.init(chartRef.current)
        const chart1 = echarts.init(chartRef1.current)
        // 设置图表实例的配置项和数据
        chart.setOption(my_options.option1)
        chart1.setOption(my_options.option2)
        return () => {
            // myChart.dispose() 销毁实例。实例销毁后无法再被使用
            chart.dispose()
            chart1.dispose()
        }
    },[])
  return (
    <div style={{display:"flex"}}>
        Ecahrts
        <div style={{width:"600px",height:"400px"}} ref={chartRef}></div>
        <div style={{width:"600px",height:"400px"}} ref={chartRef1}></div>
    </div>
  )
}

export default Ecahrts