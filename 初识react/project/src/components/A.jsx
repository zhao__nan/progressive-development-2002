import React, { Component } from 'react'
import Event_bus from '../utils/Event_bus'

export class A extends Component {
    state={
        arr:[1,2,3,4,5]
    }
    btn = () => {
        console.log(Event_bus);
        Event_bus.emit("arr",this.state.arr)
    }
  render() {
    return (
      <div>
        A
        <button onClick={this.btn}>点我传数据</button>
      </div>
    )
  }
}

export default A
