import React, { Component } from 'react'

export class My_children extends Component {
  render() {
    console.log(this.props.children);
    return (
      <div>
        {
            this.props.children.map((item,index)=>{
                if (item.type) {
                    return <item.type key={index}>{item.props.children}</item.type>
                }else{
                    return <div key={index}>{item}</div>
                }
            })
        }
      </div>
    )
  }
}

export default My_children
