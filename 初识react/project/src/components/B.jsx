import React, { Component } from 'react'
import Event_bus from '../utils/Event_bus'

export class B extends Component {
    state={
        data:[]
    }
    componentDidMount(){
        Event_bus.addListener("arr",(value)=>{
            console.log(value);
            this.setState({data:value})
        })
    }
  render() {
    return (
      <div>
        B
        {
            this.state.data.map((item)=>(<div key={item}>{item}</div>))
        }
      </div>
    )
  }
}

export default B
