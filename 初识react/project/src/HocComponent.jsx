import React , { Component }from 'react'

export class Sub_1 extends Component {
    render() {
        return (
        <div>
            <p>x:{this.props.x},y:{this.props.y}</p>
        </div>
        )
    }
}

export class Sub_2 extends Component {
    render() {
        return (
        <div>
            <button>x:{this.props.x},y:{this.props.y}</button>
        </div>
        )
    }
}

// 定义一个高级组件 以组件作为参数 并且return 出来一个组件
const Hoc_Resize = (Com) => {
    // Com 是我接受组件的行参
    return class HocComponent extends Component {
        state={
            x:document.documentElement.clientWidth,
            y:document.documentElement.clientHeight
        }
        componentDidMount(){
            //在挂载是监听resize事件 如果触发 及时更改x和y
            window.addEventListener("resize",this.setXY)
        }
        setXY = () => {
            this.setState({
                x:document.documentElement.clientWidth,
                y:document.documentElement.clientHeight
            })
        }
        componentWillUnmount(){
             //在卸载是监听resize事件 如果触发 及时更改x和y
            window.addEventListener("resize",this.setXY)
        }
        render() {
            return (
                <Com {...this.props} {...this.state}>
                    
                </Com>
            )
        }
    }
}

// 调用高阶组件
const A = Hoc_Resize(Sub_1)
const B = Hoc_Resize(Sub_2)

function HocComponent() {
  return (
    <div>
      HocComponent
      <A/>
      <B/>
    </div>
  )
}

export default HocComponent
