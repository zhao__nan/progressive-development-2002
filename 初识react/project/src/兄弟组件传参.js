import React, { Component } from 'react'
import A from './components/A'
import B from './components/B'
export class App extends Component {
  render() {
    return (
      <div>
        App
        <A></A>
        <B></B>
      </div>
    )
  }
}

export default App