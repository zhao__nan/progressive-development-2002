import React, { Component } from 'react'
import Index1 from './components/Index1'
import my_context from './utils/my_context'
export class App extends Component {
  state={
    obj:{
      name:"张三"
    }
  }
  render() {
    return (
      <div>
        app
        <my_context.Provider value={this.state.obj}>
          <Index1></Index1>
        </my_context.Provider>
       
      </div>
    )
  }
}

export default App
