// import React, { Component ,createRef} from 'react'

// export class App extends Component {
//   constructor(){
//     super()
//     this.my_span = createRef()
//   }
//   btn = () => {
//     // 1. 字符串获取
//     // console.log(this.refs.my_div);
//     // 2. 回调函数获取
//     // console.log(this.my_p);
//     // 3. 使用react 写好的createRef 获取
//     // console.log(this.my_span.current);
//   }
//   render() {
//     return (
//       <div>
//         app
//         <div ref="my_div">hi, my is div</div>
//         <p ref={(el)=>this.my_p = el}>hi, my is p</p>
//         <span ref={this.my_span}>hi,my is span</span>

//         <button onClick={this.btn}>点我</button>
//       </div>
//     )
//   }
// }

// export default App

import React,{useRef}from 'react'

function App() {
  const my_ref = useRef()
  return (
    <div>
      <div ref={my_ref}>hooks ref</div>
      <button onClick={()=>console.log(my_ref.current)}>点我</button>
    </div>
  )
}

export default App
