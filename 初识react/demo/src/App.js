import React ,{
  useState,//定义状态
} from 'react'
import Son from './components/Son'

function App() {
  /*
    1. str 你定义的数据 叫什么
    2. setStr 当你修改str会使用到
    3. useState() 括号里写的内容是默认值
  */
  const [str,setStr] = useState("123")
  const [arr,setArr] = useState([1,2,3,4,5])

  //定义一个函数
  const parentbtn = (data) => {
    setArr(data)
  }
  // data(){
  //   str:"123"
  // }
  // this.str = "456"
  let my_son = {
    arr,
    parentbtn
  }
  return (
    <div>
      App
      <p>{str}</p>
      <Son {...my_son} ></Son>
    
    </div>
  )
}

export default App

