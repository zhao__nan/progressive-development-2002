import React, { Component } from 'react'
import My_sonState from './components/My_sonState';

export class My_State extends Component {
    constructor(){
        super()
        console.log("state_constructor");
        //类组件定义数据
        this.state={
            arr:[1,2,3,4,5]
        }
    }
    //  刚进页面执行的生命周期 通常用来请求数据 挂载dom
    componentDidMount(){
        console.log("state_componentDidMount");
    }

    //更新dom时触发的生命周期
    componentDidUpdate(){
        console.log("state_componentDidUpdate");

    }

    //类组件定义es5函数
    btn(){
        console.log("你点击了es5");
        this.setState({
            arr:[6,7,8,9,10]
        })
    }
    // 类组件定义es6函数
    btn1 = () => {
        console.log("你点击了es6");
        // this.state.arr = [6,7,8,9,10]
        //类组件修改state的状态
        this.setState({
            arr:[6,7,8,9,10]
        })
    }
    // 卸载执行的生命周期
    componentWillUnmount(){
        console.log("state_componentWillUnmount");
    }
    render() {
        console.log("state_render");
        // console.log(this);
        let {arr} = this.state
        return (
            <div className='my_state'>
                My_State
                <My_sonState arr={arr} btn1={this.btn.bind(this)}/>
                {/* {
                    arr.map(item=>(<div key={item}>{item}</div>))
                } */}
                {/* <button onClick={this.btn.bind(this)}>点我更改数据</button> */}
                {/* <button onClick={this.btn1}>点我更改数据</button> */}
            </div>
        )
    }
}

export default My_State

// rce 快速出来一个类组件
// rfce 快速出来一个函数组件