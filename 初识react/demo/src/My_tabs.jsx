import React ,{
    useState,//定义本组件的状态
    useEffect,//模拟类组件的生命周期
    useCallback,
    useContext,
    useReducer,
    useMemo,
    useRef,
    
} from 'react'
import axios from 'axios';
import { Tabs } from 'antd'
import My_SonTabs from './components/My_SonTabs';

const { TabPane } = Tabs;;
function My_tabs() {
    /*
       useEffect() 
       第一个参数是一个函数
       第二个参数是一个数组，如果你第二个参数是一个空数组，代表刚进页面触发
    */
   const [data,setData] = useState([])
   const [childrenData,setChildrenData] = useState([])
   useEffect(()=>{
    axios.get("/list").then(res=>{
        //赋值数据给data
        setData(res.data.list)
        setChildrenData(res.data.list[0].children)
        return () =>{
            // 在这里写的code 就相当于是类组件的卸载生命周期
        }
    })
   },[])
   const onChange = (key) => {
        console.log(key);
        setChildrenData(data[key].children)
    };
   console.log(data);
    return (
        <div>
            <Tabs defaultActiveKey="0" onChange={onChange}> 
                {
                    data&&data.map((item,index)=>{
                        return  <TabPane tab={item.title} key={index}>
                           <My_SonTabs />
                        </TabPane>
                    })
                }
            </Tabs>
        </div>
    )
}

export default My_tabs