import React from 'react'
import PropTypes from 'prop-types'
function My_SonTabs({childrenData}) {
  return (
    <div>
      {
        childrenData
      }
         {/* {
            childrenData&&childrenData.map((val,i)=>{
                return <div key={i}>
                    <p>
                        {val.name}
                        <img src={val.img} alt="" />
                    </p>
                </div>
            })
        } */}
    </div>
  )
}
//子组件设置默认参数 如果父组件传了 用父组件的 如果没传用我自己的defaultProps
My_SonTabs.defaultProps = {
  // 校验父组件传过来的参数的类型
  childrenData:"1234"
}
My_SonTabs.propTypes = {
  // 校验父组件传过来的参数的类型    isRequired必传项
  childrenData: PropTypes.string.isRequired
}


export default My_SonTabs