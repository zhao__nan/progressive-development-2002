import React, { Component } from 'react'

export class My_sonState extends Component {
    constructor(props){
        super(props)
        console.log("son_constructor");
        console.log(props);
        // this.state={
        //     arr:props.arr
        // }
    }
    // 性能优化 这个钩子函数 返回一个布尔值 如果是true 子组件正常渲染 如果是flase 下面的代码不执行
    shouldComponentUpdate(){
        return true
    }
    componentDidMount(){
        console.log("son_componentDidMount");
    }

    componentDidUpdate(){
        console.log("son_componentDidUpdate");
    }

    componentWillUnmount(){
        console.log("son_componentWillUnmount");
    }
    btn(){    
        this.props.btn1()
    }
    render() {
        console.log("son_render");
        let {arr} = this.props
        return (
        <div>
            {
                arr.map(item=>(<div key={item}>{item}</div>))
            }
            <button onClick={this.btn.bind(this)}>点我更改数据</button>
            My_sonState
        </div>
        )
    }
}

export default My_sonState
