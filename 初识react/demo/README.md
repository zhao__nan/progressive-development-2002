#### 父传子

1. 建立你的子组件

2. 引入你的子组件

```
import Son from './components/Son'
```

3. 以标签形式渲染你的子组件 

```
 <Son></Son>
```

4. 在你的标签上 传入你要传入的值 eg:arr={arr}

```
<Son arr={arr}></Son>
```

5. 在你的子组件也就是你的Son 接受你父组件传过来的参数 props ,并渲染
```
function Son(props) {
    console.log(props);
    let {arr} = props
  return (
    <div className='son'>
        Son
        {
            arr.map((item,i)=>(<div key={i}>{item}</div>))
        }
    </div>
  )
}
```
### 生命周期

#### 挂载阶段
钩子函数	            触发时机	            作用
constructor	   创建组件时，最先执行	  1. 初始化state 2. 创建 Ref 3. 使用 bind 解决 this 指向问题等
render	       每次组件渲染都会触发	    渲染UI（注意： 不能调用setState() ）
componentDidMount	组件挂载（完成DOM渲染）后	  1. 发送网络请求 2.DOM操作
#### 更新阶段
钩子函数	             触发时机	            作用
render	            每次组件渲染都会触发	  渲染UI（与 挂载阶段 是同一个render）
componentDidUpdate	组件更新（完成DOM渲染）后	DOM操作，可以获取到更新后的DOM内容，不要直接调用setState

#### 卸载阶段
componentWillUnmount	组件卸载（从页面中消失）	执行清理工作（比如：清理定时器等）

#### 性能优化 这个钩子函数 返回一个布尔值 如果是true 子组件正常渲染 如果是flase 下面的代码不执行
```
 shouldComponentUpdate(){
    return true
}
```

### 函数组件和类组件的区别

#### 函数组件 
* 没有this指向 没有state状态 没有自己的生命周期

#### 类组件
* 组件有this指向 有自己的状态 有自己的生命周期