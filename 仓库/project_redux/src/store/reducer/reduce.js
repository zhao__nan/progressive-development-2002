//rxreducer

const initialState = {
    data:[],
    childrenData:[]
}

export default (state = initialState, { type, payload }) => {
    let newState =JSON.parse(JSON.stringify(state))

    switch (type) {
        case "GET_LIST":
            newState.data = payload
            newState.childrenData = payload[0].children
            return newState

        case "CHECK_CHILDRENDATA":
            newState.childrenData = newState.data[payload].children
            return newState
            
        default:
            return newState
    }
}
