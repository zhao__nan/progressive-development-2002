//rxreducer

const initialState = {
    data:JSON.parse(window.localStorage.getItem("data")) || [],
}

export default (state = initialState, { type, payload }) => {
    let newState =JSON.parse(JSON.stringify(state))

    switch (type) {
        case "ADD_DATA":
            newState.data.push(payload)
            window.localStorage.setItem("data",JSON.stringify(newState.data))
            return newState

        case "DEL_DATA":
            newState.data.splice(payload,1)
            window.localStorage.setItem("data",JSON.stringify(newState.data))
            return newState

        case "EDIT_DATA":
            newState.data[payload.count] = payload.value
            window.localStorage.setItem("data",JSON.stringify(newState.data))
            return newState
            
        default:
            return newState
    }
}
