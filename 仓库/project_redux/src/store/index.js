// 搭建你的仓库

import {legacy_createStore,combineReducers,applyMiddleware} from "redux"

import thunk from "redux-thunk"
import logger from "redux-logger"

import reduce from "./reducer/reduce"
import todu_reduce from "./reducer/todu_reduce"

const All_reducer = combineReducers({
    reduce,
    todu_reduce
})

export default legacy_createStore(All_reducer,applyMiddleware(thunk,logger))