import React ,{useEffect} from 'react'
import {Modal,Form,Input,Button} from "antd"
function My_model({isModalVisible,handleOk,handleCancel,edit_value}) {
    const [form] = Form.useForm()

    useEffect(()=>{
        // 设置回显值
        form.setFieldsValue({
            value:edit_value
        })
    },[edit_value])

    const onFinish = (values) => {
        handleOk(values.value)
    };
    
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
  return (
    <div>
        <Modal forceRender footer={null} title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <Form
            form={form}
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 16,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            >
            <Form.Item
                label="Value"
                name="value"
                rules={[
                {
                    required: true,
                    message: 'Please input your value!',
                },
                ]}
            >
                <Input style={{width:"200px"}} />
            </Form.Item>


            <Form.Item
                wrapperCol={{
                offset: 8,
                span: 16,
                }}
            >
                <Button type="primary" onClick={handleCancel}>
                    取消
                </Button>
                <Button type="primary" htmlType="submit">
                    确认
                </Button>
            </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

export default My_model