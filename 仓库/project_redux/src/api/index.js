import axios from "axios"

// export let get_list1 = () =>{
//     return  (dispatch) => {
//         axios.get("/list").then(res=>{
//             dispatch({
//                 type:"GET_LIST",
//                 payload:res.data.list
//             })
//         })
//     }
// }

export let get_list = () =>{
    return async (dispatch) => {
        let {data} = await axios.get("/list")
        dispatch({
            type:"GET_LIST",
            payload:data.list
        })
    }
}

export let check_childrenData = (key) => {
    return {
        type:"CHECK_CHILDRENDATA",
        payload:key
    }
}

export let add_data = (value) => {
    return {
        type:"ADD_DATA",
        payload:value
    }
}

export let del_data = (index) => {
    return {
        type:"DEL_DATA",
        payload:index
    }
}

export let edit_data = (obj) => {
    return {
        type:"EDIT_DATA",
        payload:obj
    }
}
