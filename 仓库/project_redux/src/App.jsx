import React ,{useEffect} from 'react'
import {useDispatch,useSelector} from "react-redux"
import * as api from "./api/index"

import { Tabs } from 'antd';
const { TabPane } = Tabs;

function App() {
    const dispatch = useDispatch()
    const data = useSelector((state)=>{
        return state.reduce.data
    })
    const childrenData = useSelector((state)=>{
        return state.reduce.childrenData
    })
    useEffect(()=>{
        // axios.get("/list").then(res=>{
        //     // console.log(res.data.list);
        //     dispatch({
        //         type:"GET_LIST",
        //         payload:res.data.list
        //     })
        //    })

       dispatch(api.get_list())
    },[dispatch])
    const onChange = (key) => {
        dispatch(api.check_childrenData(key))
    };
    console.log(data);
    return (
        <div>
             <Tabs defaultActiveKey="1" onChange={onChange}>
                {
                    data&&data.map((item,index)=>{
                        return  <TabPane tab={item.name} key={index}>
                            {
                                childrenData&&childrenData.map((val,i)=>{
                                    return <div key={i}>
                                        <p>name:{val.name}</p>
                                        <img src={val.img} alt="" />
                                    </div>
                                })
                            }
                        </TabPane>
                    })
                }
            </Tabs>
        </div>
    )
}

export default App