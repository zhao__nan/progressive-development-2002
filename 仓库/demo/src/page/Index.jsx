import React from 'react'
import {  PieChartOutlined, UserOutlined,DesktopOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import { useState } from 'react';
import {Outlet,NavLink} from "react-router-dom"
const { Header, Content, Footer, Sider } = Layout;

function Index() {
  const [collapsed, setCollapsed] = useState(false);
  function getItem(label, key, icon, children) {
    return {
      key,
      icon,
      children,
      label,
    };
  }
  
  const items = [
    getItem(<NavLink to={"/index/home"}>首页</NavLink>, '1', <PieChartOutlined />),
    getItem('应用管理', 'sub1', <UserOutlined />, [
      getItem(<NavLink to={"/index/my_Application"}>我的应用</NavLink>, '3'),
      getItem(<NavLink to={"/index/recycle"}>回收站</NavLink>, '4'),
    ]),
    getItem('帐号中心', 'sub2', <DesktopOutlined />, [
      getItem(<NavLink to={"/index/authentication"}>帐号信息</NavLink>, '6'),
      getItem(<NavLink to={"/index/contact"}>帐号认证</NavLink>, '7'),
      getItem(<NavLink to={"/index/information"}>帐号联系人</NavLink>, '8'),
    ]),
    getItem('工单', 'sub3', <UserOutlined />, [
      getItem(<NavLink to={"/index/createOrder"}>创建工单</NavLink>, "9"),
      getItem(<NavLink to={"/index/OrderList"}>工单列表</NavLink>, '10'),
    ]),
    getItem(<NavLink to={"/index/dataVisualization"}>数据可视化</NavLink>, '11', <PieChartOutlined />),
  ];
  
  return (
    <div className='index'>
       <Layout
        style={{
          minHeight: '100vh',
        }}
      >
        <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
          <div className="logo" />
          <Menu defaultSelectedKeys={['1']} mode="inline" items={items} />
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
             
              padding: "10 0",
            }}
          />
          <Content
            style={{
              margin: '0 16px',
            }}
          >
            <div
              className="site-layout-background"
              style={{
                width:"100%",
                height:"100%",
                padding: 24,
                minHeight: 360,
                // background:"#ccc"
              }}
            >
              <Outlet/>
            </div>
          </Content>
          <Footer
            style={{
              textAlign: 'center',
              height:"30px"
            }}
          >
          
          </Footer>
        </Layout>
      </Layout>
    </div>
  )
}

export default Index