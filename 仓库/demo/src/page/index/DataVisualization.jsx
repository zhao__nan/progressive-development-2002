import React ,{useEffect,useRef} from 'react'
import * as echarts from 'echarts';
import * as option from "../../utils/my_option"
function DataVisualization() {
  const barmychart = useRef()
  const linemychart = useRef()
  const piemychart = useRef()
  useEffect(()=>{
    // 柱状图
    //echarts.init 初始化 括号里写我获取的盒子
    let barmyChart = echarts.init(barmychart.current)
    // setOption 绘画ecahrts
    barmyChart.setOption(option.baroption);
    // 折线图
    let linemyChart = echarts.init(linemychart.current)
    linemyChart.setOption(option.lineoption)
    //饼图
    let piemyChart = echarts.init(piemychart.current)
    piemyChart.setOption(option.pieoption)

    return () => {
        // 离开组件时销毁掉
        barmyChart.dispose()
        linemyChart.dispose()
        piemyChart.dispose()
    }
  },[])
  return (
    <div >
      <p>数据可视化</p>
      <div style={{display:"flex"}}>
        <div style={{width:"400px",height:"300px"}} ref={barmychart}></div>
        <div style={{width:"400px",height:"300px"}} ref={linemychart}></div>
        <div style={{width:"400px",height:"300px"}} ref={piemychart}></div>
      </div>
    </div>
  )
}

export default DataVisualization