import React ,{ useState } from 'react'
import { PlusOutlined } from '@ant-design/icons';
import {
  ProForm,
  ProFormCheckbox,
  ProFormDatePicker,
  ProFormDateTimePicker,
  ProFormSelect,
  ProFormText,
  ProFormTextArea,
  StepsForm,
} from '@ant-design/pro-components';
import { Button, message, Modal } from 'antd';

function CreateOrder() {
  const [visible, setVisible] = useState(false);
  const waitTime = (time = 100) => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(true);
      }, time);
    });
  };
  
  return (
    <div>
       <Button type="primary" onClick={() => setVisible(true)}>
        <PlusOutlined />
        分步表单新建
      </Button>

      <StepsForm
        onFinish={async (values) => {
          console.log(values);
          await waitTime(1000);
          setVisible(false);
          message.success('提交成功');
        }}
        // formProps={{
        //   validateMessages: {
        //     required: '此项为必填项',
        //   },
        // }}
        // 分布表单外展示的内容
        stepsFormRender={(dom, submitter) => {
          return (
            <Modal
              title="分步表单1111"
              width={800}
              onCancel={() => setVisible(false)}
              visible={visible}
              footer={submitter}
              destroyOnClose
            >
              {dom}
            </Modal>
          );
        }}
      >
        {/* 第一个表单 */}
        <StepsForm.StepForm
          name="base"
          title="创建实验"
          onFinish={ () => {
            // await waitTime(2000);
            return true;
          }}
        >
          <ProFormText
            name="name"
            width="md"
            label="实验名称"
            tooltip="最长为 24 位，用于标定的唯一 id"
            placeholder="请输入名称"
            rules={[{ required: true ,message:"请输入实验名称"}]}
          />
          <ProFormTextArea
            rules={[{ required: true ,message:"1111"}]}
            name="remark" 
            label="备注" 
            width="lg" 
            placeholder="请输入备注" />
        </StepsForm.StepForm>
        {/* 第二个表单 */}
        <StepsForm.StepForm name="checkbox" title="设置参数">
          
          <ProForm.Group>
            <ProFormText width="md" name="dbname" label="业务 DB 用户名" />
            <ProFormCheckbox.Group
              name="checkbox"
              label="迁移类型"
              options={['完整 LOB', '不同步 LOB', '受限制 LOB']}
            />
          </ProForm.Group>
        </StepsForm.StepForm>
        {/* 第三个表单 */}
        <StepsForm.StepForm name="time" title="发布实验">
          <ProFormCheckbox.Group
            name="checkbox"
            label="部署单元"
            rules={[
              {
                required: true,
              },
            ]}
            options={['部署单元1', '部署单元2', '部署单元3']}
          />
          <ProFormSelect
            label="部署分组策略"
            name="remark"
            rules={[
              {
                required: true,
              },
            ]}
            width="md"
            initialValue="1"
            options={[
              {
                value: '1',
                label: '策略一',
              },
              { value: '2', label: '策略二' },
            ]}
          />
          <ProFormSelect
            label="Pod 调度策略"
            name="remark2"
            width="md"
            initialValue="2"
            options={[
              {
                value: '1',
                label: '策略一',
              },
              { value: '2', label: '策略二' },
            ]}
          />
        </StepsForm.StepForm>
      </StepsForm>
    </div>
  )
}

export default CreateOrder