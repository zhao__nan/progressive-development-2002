import React ,{useEffect,useState}from 'react'
import {useDispatch,useSelector} from "react-redux"
import * as api from "../../../api/index"
import { Spin, Space, Table, Select, Button,Badge ,Popconfirm,message, Input} from 'antd';
import My_model from '../../../components/My_model';
import PopconfirmCom from '../../../components/PopconfirmCom';
import Search from 'antd/lib/transfer/search';


function OrderList() {
  const dispatch = useDispatch()
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [edit_value,setEdit_value] = useState({})
  const [check_data,setCheck_data] = useState([])
  const table_loading = useSelector((state)=>{
    return state.reduce.table_loading
  })

  const data = useSelector((state)=>{
    return state.reduce.data
  })
  // 设置state 前面小点
  const set_status = (value) =>{
    switch (value) {
      case "已完成":
        return "success"
      case "未完成":
        return "error"
      case "进行中":
        return "warning"
    }
  }

  const columns = [
    {
      title: 'Key',
      dataIndex: 'key',
      key: 'key',
      render: (text) => <a>{text}</a>,
      // 根据key 升序降序
      defaultSortOrder: 'ascend',
      sorter: (a, b) => a.key - b.key,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: '性别',
      dataIndex: 'sex',
      key: 'sex',
    },
    {
      title: 'Ctiy',
      dataIndex: 'city',
      key: 'city',
    },
    {
      title: 'State',
      dataIndex: 'state',
      key: 'state',
      render: (_, record) => (
        <Space size="middle">
          <Badge status={set_status(record.state)}></Badge>
          <span>{record.state}</span>
        </Space>
      )
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <Button onClick={()=>edit_data(record)}>edit</Button>
          {/* 删除二级弹框 */}
          {/* <Popconfirm
            title="确认删除吗?"
            onConfirm={()=>confirm(record)}
            onCancel={cancel}
            okText="Yes"
            cancelText="No"
          >
            <Button>Delete</Button>
          </Popconfirm> */}
          <PopconfirmCom title="删除" fn={()=>del_data(record)} record={record} ></PopconfirmCom>
          
        </Space>
      )
    },
  ];
  //请求数据
  useEffect(()=>{
    dispatch(api.get_table())
  },[dispatch])

  //删除函数
  const del_data = (record) =>{
    dispatch(api.del_data(record))
  }
  // 筛选数据
  const select_data = (value) => {
    console.log(value);
    dispatch(api.select_data(value))
  }
  // 添加
  const add_data = () =>{
    setEdit_value({})
    setIsModalVisible(true)
  }

  // 编辑
  const edit_data = (record) =>{
    setEdit_value(record)
    setIsModalVisible(true)
  }
  const handleOk = (values) => {
    console.log(values);
    edit_value.key?dispatch(api.edit_data(values)):dispatch(api.add_data(values))
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  
  // 单选全选
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setCheck_data(selectedRowKeys)
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name,
    }),
  };
  
  const all_del_data = () => {
    dispatch(api.all_del_data(check_data))
  }
  
  let my_model = {
    isModalVisible,
    handleOk,
    handleCancel,
    data,
    edit_value
  }
const search=(value)=>{
  console.log(value);
  if(value===""){
    message.error("没输入值")
  }
  dispatch(api.search(value))
} 
  return (
    <div>
       <Spin spinning={table_loading}>
        <div style={{display:"flex"}}>
          <Button onClick={()=>add_data()}>新增</Button>
          <Select  
            style={{width: 120,}}
            onChange={select_data}
            placeholder="请筛选数据"
            >
            <Select.Option value="已完成">已完成</Select.Option>
            <Select.Option value="进行中">进行中</Select.Option>
            <Select.Option value="未完成">未完成</Select.Option>
          </Select>
          <Input 
            type="text"  
            style={{width:"200px"}} 
            placeholder="请输入你要搜索的值"
            onPressEnter={(e)=>search(e.target.value)}
            />
          {
            check_data.length>1 &&<PopconfirmCom title="全部删除" fn={all_del_data}></PopconfirmCom>
          }
          {
            // check_data.length>1 && <Button onClick={all_del_data}>全部删除</Button>
          }
       
        </div>
        
        <Table pagination={{
          pageSize:7
        }} 
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        columns={columns} 
        dataSource={data} />
       </Spin>
        <My_model {...my_model}></My_model>
    </div>
  )
}

export default OrderList