// 搭建仓库的代码

import {
    legacy_createStore as createStore, // 创建仓库
    combineReducers,//合并仓库
    applyMiddleware,// 处理中间件
} from "redux"

// 引入的中间件
import thunk from "redux-thunk"
import logger from "redux-logger"

// 引入我的仓库
import reduce from "./reducer/reduce"

const All_reducer = combineReducers({
    reduce
})

// 创建我的仓库
const store = createStore(All_reducer,applyMiddleware(thunk,logger))

export default store