// rxreducer

import {message} from "antd"
const initialState = {
    data:[],
    copy_data:[],
    table_loading:true
}

export default (state = initialState, { type, payload }) => {
    let newState = JSON.parse(JSON.stringify(state))
    switch (type) {
        case "GET_TABLE":
            newState.data = payload.data
            newState.copy_data = payload.data
            newState.table_loading = payload.loading
            return newState

        // 删除
        case "DEL_DATA":
            newState.data.forEach((item,index)=>{
                if(item.id === payload.id){
                    newState.data.splice(index,1)
                    newState.copy_data.splice(index,1)
                    message.success("删除成功")
                }
            })
            return newState

         // 筛选
         case "SELECT_DATA":
            newState.data = newState.copy_data.filter(item => item.state === payload)
            return newState
        
        // 添加
         case "ADD_DATA":
            newState.data.push(payload)
            message.success("添加成功")
            return newState
        
        // 编辑
        case "EDIT_DATA":
            newState.data.forEach((item,index)=>{
                if (item.id === payload.id) {
                    newState.data[index] = {...payload}
                    message.success("编辑成功")
                }
            })
            return newState
        // 全部删除
        case "ALL_DEL_DATA":
            for (let index = newState.data.length-1; index>= 0 ; index --) {
                for (let i = 0; i < payload.length; i++) {
                    if (newState.data[index].key === payload[i]) {
                        newState.data.splice(index,1)
                    }
                }
            }

            return newState
        //  搜索
        case "SEARCH_DATA":
           newState.data=newState.copy_data.filter((item)=>{
            return item.name.includes(payload)
           })

            return newState
            
        default:
            return newState
    }
}
