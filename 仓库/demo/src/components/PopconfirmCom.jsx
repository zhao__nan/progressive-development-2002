import React from 'react'
import {Popconfirm,message,Button} from "antd"
function PopconfirmCom({title,fn,record}) {
    const confirm = () => {
        fn(record)
    };
      
    const cancel = (e) => {
        console.log(e);
        message.error('Click on No');
    };
    
    return (
        <div>  
            <Popconfirm
                title={`确认${title}吗?`}
                onConfirm={()=>confirm()}
                onCancel={cancel}
                okText="Yes"
                cancelText="No"
            >
                <Button>{title}</Button>
            </Popconfirm>
        </div>
  )
}

PopconfirmCom.defaultProps = {
    title:"哈哈哈",
    fn:()=>{
        console.log(123);
    }
}
export default PopconfirmCom