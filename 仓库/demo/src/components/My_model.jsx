import React ,{useState,useEffect} from 'react'
import {Modal,Button, Select, Form, Input,Radio,Cascader} from "antd"
import {form_option} from "../utils/my_option"

function My_model({isModalVisible,handleOk,handleCancel,data,edit_value}) {
    // 获取表单实例
    // alert("")
    const [form] = Form.useForm()
    const [value,setValue] = useState("")
    useEffect(()=>{
        // 设置表单回显值
        if (edit_value.key) {
            form.setFieldsValue({
                ...edit_value
            })
        }else {
            form.setFieldsValue({
                name:"",
                age:"",
                sex:"",
                city:"",
                state:""
            })
        }
    },[edit_value])
    const onChange = (value) => {
        setValue(value[2])
        console.log(value);
      };
    const onFinish = (values) => {
        values.city = values.city[2]
        if(!edit_value.key){
            values.key = data.length+1
            values.id = new Date().getTime()
        }else{
            values.key = edit_value.key
            values.id = edit_value.id
        }
        console.log('Success:', values);
        handleOk(values)
      
    };
    
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
  return (
    <div>
        
        <Modal forceRender footer={null} title={edit_value.key?"Edit modal": "Add modal"} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <Form
            form={form}
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 16,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            >
            <Form.Item
                label="Name"
                name="name"
                rules={[
                {
                    required: true,
                    message: 'Please input your username!',
                },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Age"
                name="age"
                rules={[
                {
                    required: true,
                    message: 'Please input your age!',
                },
                {
                    pattern:/^[1-9]\d{0,1}$/,
                    message:"请输入合适年龄"
                }
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="Sex"
                name="sex"
                rules={[
                {
                    required: true,
                    message: 'Please input your sex!',
                },
                ]}
            >
                 <Radio.Group name="radiogroup">
                    <Radio value={"男"}>男</Radio>
                    <Radio value={"女"}>女</Radio>
                 </Radio.Group>
               
            </Form.Item>

            <Form.Item
                label="City"
                name="city"
                rules={[
                {
                    required: true,
                    message: 'Please input your city!',
                },
                ]}
            >
               <Cascader value={value} options={form_option} onChange={onChange} placeholder="Please select" />
            </Form.Item>

            <Form.Item
                label="State"
                name="state"
                rules={[
                {
                    required: true,
                    message: 'Please input your state !',
                },
                ]}
            >
                 <Select >
                    <Select.Option value={"已完成"}>已完成</Select.Option>
                    <Select.Option value={"进行中"}>进行中</Select.Option>
                    <Select.Option value={"未完成"}>未完成</Select.Option>
                </Select>
            </Form.Item>

            <Form.Item
                wrapperCol={{
                offset: 8,
                span: 16,
                }}
            >
                <Button type="primary" style={{marginRight:"30px"}} onClick={handleCancel}>
                    取消
                </Button>
                <Button type="primary" htmlType="submit">
                    确认
                </Button>
            </Form.Item>
            </Form>
        </Modal>
    </div>
  )
}

export default My_model