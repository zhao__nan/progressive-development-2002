import { lazy } from "react";

export let mainRouter = [
    {
        path:"/index/home",
        element:lazy(()=>import("../page/index/Home"))
    },
    {
        path:"/index/my_Application",
        element:lazy(()=>import("../page/index/Application/My_Application"))
    },
    {
        path:"/index/recycle",
        element:lazy(()=>import("../page/index/Application/Recycle"))
    },
    {
        path:"/index/authentication",
        element:lazy(()=>import("../page/index/AccountCenter/Authentication"))
    },
    {
        path:"/index/contact",
        element:lazy(()=>import("../page/index/AccountCenter/Contact"))
    },
    {
        path:"/index/information",
        element:lazy(()=>import("../page/index/AccountCenter/Information"))
    },
    {
        path:"/index/createOrder",
        element:lazy(()=>import("../page/index/repairOrder/CreateOrder"))
    },
    {
        path:"/index/OrderList",
        element:lazy(()=>import("../page/index/repairOrder/OrderList"))
    },
    {
        path:"/index/dataVisualization",
        element:lazy(()=>import("../page/index/DataVisualization"))
    },
    
]

const routes = [
    {
        path:"/index",
        element:lazy(()=>import("../page/Index")),
        children:mainRouter
    },
    {
        path:"/login",
        element:lazy(()=>import("../page/Login"))
    },
    {
        path:"*",
        element:lazy(()=>import("../page/Eorry"))
    },
    {
        from:"/",
        to:"/index/home"
    }
]

export default routes