import axios from "axios"

export let get_table = () => {
    return async(dispatch)=>{
        let res = await axios.get("/table")
        if (res.status === 200) {
            dispatch({
                type:"GET_TABLE",
                payload:{
                    data:res.data.list,
                    loading:false
                }
            })
        }
    }
}

// 删除
export let del_data = (record) => {
    return {
        type:"DEL_DATA",
        payload:record
    }
}

// 筛选
export let select_data = (value) => {
    return {
        type:"SELECT_DATA",
        payload:value
    }
}
// 添加
export let add_data = (values) => {
    return {
        type:"ADD_DATA",
        payload:values
    }
}

// 编辑
export let edit_data =(values) =>{
    return {
        type:"EDIT_DATA",
        payload:values
    }
}

// 全部删除
export let all_del_data =(arr) =>{
    return {
        type:"ALL_DEL_DATA",
        payload:arr
    }
}

//模糊搜索
export let search =(value) =>{
    return {
        type:"SEARCH_DATA",
        payload:value
    }
}