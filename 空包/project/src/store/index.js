import {
    legacy_createStore,
    applyMiddleware,
    combineReducers
} from "redux"

import thunk from "redux-thunk"
import logger from "redux-logger"

import reduce from "./reducer/reduce"

const All_Reducer = combineReducers({
    reduce
})

export default legacy_createStore(All_Reducer,applyMiddleware(thunk,logger))
