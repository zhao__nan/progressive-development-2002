//rxreducer
import {message} from "antd"

const initialState = {
    table_data:[],
    copy_data:[],
    table_loading:true
}

export default (state = initialState, { type, payload }) => {
    let newState = JSON.parse(JSON.stringify(state))
    switch (type) {

        case "GET_TABLE":
            newState.table_data = payload
            newState.copy_data = payload
            newState.table_loading = false
            return newState
            
        case "DEL_DATA":
            newState.table_data.forEach((item,index)=>{
                if (item.id === payload.id) {
                    newState.table_data.splice(index,1)
                    message.success("删除成功")
                }
            })
            return newState

        case "ADD_DATA":
            newState.table_data.push(payload)
            newState.copy_data.push(payload)
            message.success("添加成功")
            return newState

        case "SELECT_DATA":
            newState.table_data = newState.copy_data.filter(item=>item.state === payload)
            return newState
        
        case "EDIT_DATA":
            newState.table_data.forEach((item,index)=>{
                if (item.id === payload.id) {
                    newState.table_data[index] = {...payload}
                }
            })
            return newState
        
        case "SORT_DATA":
            if (payload === "1") {
               let data = newState.copy_data.sort((a,b)=>{
                    return a.key - b.key
                })
                newState.table_data = data
            }else{
                let data = newState.copy_data.sort((a,b)=>{
                    return b.key - a.key
                })
                console.log(data);
                newState.table_data = data
            }

            return newState
        case "ALL_DEL_DATA":
            payload.forEach((item,index)=>{
                newState.table_data.forEach((val,i)=>{
                    if (item === val.key) {
                        newState.table_data.splice(i,1)
                    }
                })
            })
            return newState
            
        default:
            return newState
    }
}
