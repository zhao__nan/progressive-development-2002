import React ,{useEffect,useState} from 'react'
import {useDispatch,useSelector} from "react-redux"
import {Spin, Space, Table,Image, Button,Popconfirm,message,Select,} from "antd"
import * as api from "../../api/index"
import { PlusOutlined } from '@ant-design/icons';
import My_Form from '../../components/My_Form';

function My_table() {
  const dispatch = useDispatch()
  const [check_data,setCheck_data] = useState([])
  const [visible, setVisible] = useState(false);
  const [edit_value, setEdit_value] = useState({});
  useEffect(()=>{
    dispatch(api.get_table())
  },[dispatch])

  const columns = [
    {
      title: 'Key',
      dataIndex: 'key',
      key: 'key',
      defaultSortOrder: 'ascend',
      sorter: (a, b) => a.key - b.key,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: 'Image',
      dataIndex: 'img',
      key: 'img',
      render: (_, record) => (
        <Space size="middle">
          <Image width={100} height={50} src={record.img} />
        </Space>
      ),
    },
    {
      title: 'City',
      dataIndex: 'city',
      key: 'city',
    },
    {
      title: 'State',
      dataIndex: 'state',
      key: 'state',
    },
   
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <Button onClick={()=>edit_data(record)}>edit</Button>
          <Popconfirm
            title="确认要删除吗?"
            onConfirm={()=>del_data(record)}
            onCancel={cancel}
            okText="Yes"
            cancelText="No"
          >
             <Button >Delete</Button>
          </Popconfirm>
       
        </Space>
      ),
    },
  ];
  const table_loading = useSelector((state)=>{
    return state.reduce.table_loading
  })
  const table_data = useSelector((state)=>{
    return state.reduce.table_data
  })

  const del_data = (record) => {
    dispatch(api.del_data(record))
  }
  
  const add_data = () => {
    setEdit_value({
      name:"",
      key:"",
      age:"",
      state:"",
      city:"",
      title:"新增"
    })
    setVisible(true)
  }

  const select_data = (value) =>{
    console.log(value);
    dispatch(api.select_data(value))
  }

  const edit_data = (record) => {
    setVisible(true)
    record.title ="编辑"
    setEdit_value(record)
  }

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setCheck_data(selectedRowKeys)
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name,
    }),
  };
  const cancel = (e) => {
    console.log(e);
    message.error('Click on No');
  };
  const handOk = (values) =>{
    edit_value.title === "新增"?dispatch(api.add_data(values)):dispatch(api.edit_data(values))
    
    setVisible(false)
  }

  const handCanle =() =>{
    setVisible(false)
  }
  const sort_data = (str) => {
    dispatch(api.sort_data(str))
  }
  const all_del_data = () =>{
    dispatch(api.all_del_data(check_data))
  }

  let my_form = {
    visible,
    handOk,
    handCanle,
    edit_value
  }
  return (
    <div>
      <Spin spinning={table_loading}>
        <div>
          <Button type="primary" onClick={add_data}>
            <PlusOutlined />
            新增数据
          </Button>
          <Select placeholder="请您筛选" style={{width:"100px"}} onChange={select_data}>
            <Select.Option value={"已完成"}>已完成</Select.Option>
            <Select.Option value={"进行中"}>进行中</Select.Option>
            <Select.Option value={"未完成"}>未完成</Select.Option>
          </Select>
          <Button onClick={()=>sort_data("1")}>⬆️</Button>
          <Button onClick={()=>sort_data("2")}>⬇️</Button>
          <Button onClick={all_del_data}> 批量删除</Button>
        </div>
        <Table  
          rowSelection={{
          ...rowSelection,
          }} 
          pagination={{
            pageSize:5
          }}
        columns={columns} dataSource={table_data} />
      </Spin>
      <My_Form {...my_form}></My_Form>
      
      
    </div>
  )
}

export default My_table