import React ,{useEffect,useRef} from 'react'
import axios from "../../utils/Request"
import * as echarts from 'echarts';
import {lineoption} from "../../utils/my_options"
function My_echarts() {
  const lineEcharts = useRef()
  useEffect(()=>{
    // 基于准备好的dom，初始化echarts实例
    const lineMyChart = echarts.init(lineEcharts.current);
    lineMyChart.setOption(lineoption)
    axios.get("/lineoption").then(res=>{
      lineMyChart.setOption({
        xAxis:{
          data:res.data.x
        },
        series:[{
          data:res.data.series
        }]
      })
    })
    return () => {
      lineMyChart.dispose()
    }
  },[])
  return (
    <div>
      <div ref={lineEcharts} style={{width:"500px",height:"300px"}}></div>
    </div>
  )
}

export default My_echarts