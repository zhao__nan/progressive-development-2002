import React ,{useEffect,useRef} from 'react'
import {Modal} from "antd"
import {
    ProFormSelect,
    ProFormText,
    StepsForm,
  } from '@ant-design/pro-components';
  
function My_Form({visible,handCanle,handOk,edit_value}) {
    const my_form = useRef()
    useEffect(()=>{
        my_form?.current?.forEach(item=>{
            item.current?.setFieldsValue(edit_value)
        }) 
    },[edit_value])
    const waitTime = (time = 100) => {
        return new Promise((resolve) => {
          setTimeout(() => {
            resolve(true);
          }, time);
        });
    };
    const onFinish = async (values) => {
        console.log(values);
        console.log(edit_value);
        if (edit_value.title === "新增") {
            values.id = new Date().getTime()
            values.img = "https://t7.baidu.com/it/u=4162611394,4275913936&fm=193&f=GIF"
        }else{
            values.id = edit_value.id
            values.img = edit_value.img
        }
      
        await waitTime(1000);
        handOk(values)
    
        }

  return (
    <div>
        <StepsForm
            // formRef={}
            formMapRef={my_form}
            onFinish={onFinish}
            formProps={{
            validateMessages: {
                required: '此项为必填项',
            },
            }}
            stepsFormRender={(dom, submitter) => {
            return (
                <Modal
                    forceRender
                    title="分步表单"
                    width={800}
                    onCancel={handCanle}
                    visible={visible}
                    footer={submitter}
                    destroyOnClose={true}
                    >
                {dom}
                </Modal>
            );
            }}
        >
        <StepsForm.StepForm name="user" title="用户">
          <ProFormText
            name="key"
            width="md"
            label="Key"
            placeholder="请输入key"
            rules={[{ required: true }]}
          />
          <ProFormText
            name="name"
            width="md"
            label="姓名"
            placeholder="请输入姓名"
            rules={[{ required: true }]}
          />
          <ProFormText
            name="age"
            width="md"
            label="年龄"
            placeholder="请输入年龄"
            rules={[{ required: true }]}
          />
          
        </StepsForm.StepForm>
        <StepsForm.StepForm name="state" title="状态">
        <ProFormText
            name="city"
            width="md"
            label="city"
            placeholder="请输入城市"
            rules={[{ required: true }]}
          />
        <ProFormSelect
            label="state"
            name="state"
            width="md"
            initialValue="已完成"
            rules={[{ required: true }]}
            options={[
              {
                value: '已完成',
                label: '已完成',
              },
              { 
                value: '进行中',
                label: '进行中' 
              },
              { 
                value: '未完成',
                label: '未完成' 
              }
            ]}
          />
        </StepsForm.StepForm>
      </StepsForm>
    </div>
  )
}

export default My_Form