import { lazy } from "react";

const routes = [
    {
        path:"/index",
        element:lazy(()=>import("../page/Index")),
        children:[
            {
                path:"/index/home",
                element:lazy(()=>import("../page/index/Home"))
            },
            {
                path:"/index/my_echarts",
                element:lazy(()=>import("../page/index/My_echarts"))
            },
            {
                path:"/index/my_table",
                element:lazy(()=>import("../page/index/My_table"))
            },
            {
                path:"/index/order",
                element:lazy(()=>import("../page/index/Order"))
            },
            {
                path:"/index/my",
                element:lazy(()=>import("../page/index/My"))
            }
        ]
    },
    {
        path:"*",
        element:lazy(()=>import("../page/Eorry404"))
    },
    {
        path:"/login",
        element:lazy(()=>import("../page/Login"))
    },
    {
        from:"/",
        to:"/index/home"
    }
]

export default routes