import axios from "../utils/Request"

export let get_table = () => {
    return (dispatch) => {
        axios.get("/api/table").then(res=>{
            console.log(res);
            if (res.status === 200) {
                dispatch({
                    type:"GET_TABLE",
                    payload:res.data.list
                })
            }
        })
    }
}

export let del_data = (record) => {
    return {
        type:"DEL_DATA",
        payload:record
    }
}

export let add_data = (values) => {
    return {
        type:"ADD_DATA",
        payload:values
    }
}

export let select_data = (value) => {
    return {
        type:"SELECT_DATA",
        payload:value
    }
}

export let edit_data = (value) => {
    return {
        type:"EDIT_DATA",
        payload:value
    }
}

export let sort_data = (str) => {
    return {
        type:"SORT_DATA",
        payload:str
    }
}

export let all_del_data = (arr) => {
    return {
        type:"ALL_DEL_DATA",
        payload:arr
    }
}
