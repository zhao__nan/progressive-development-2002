import React from 'react'
import {
    useLocation,
    useParams,
    useNavigate
} from "react-router-dom"

function withRouter(Com) {
  return (props)=>{
    const location = useLocation()
    const params = useParams()
    const navigate = useNavigate()
    return <Com {...props} router={{location,params,navigate}}></Com>
  }
}

export default withRouter