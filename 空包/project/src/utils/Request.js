import axios from "axios"

const instance = axios.create({
    timeout:3000
});
// Add a request interceptor
instance.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor
instance.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    const status = error.response.status
    switch (status) {
        case "304":
            console.log("重定向");
            break;
        case "404":
            console.log("页面找不到");
            break;
        case "500":
            console.log("服务器错误");
            break;
        case "200":
            console.log("成功");
            break;
        case "401":
            console.log("没有权限");
            break;
        default:
            break;
    }
    return Promise.reject(error);
});

export default instance