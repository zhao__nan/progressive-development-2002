export let lineoption = {
    xAxis: {
      type: 'category',
      data: []
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        data:[],
        type: 'pie'
      }
    ]
  };