// 配置我的路由表
// 引入路由懒加载
import { lazy } from "react";

export let mainRouter = [ 
    {
        path:"/index/home",
        element:lazy(()=>import("../page/index/Home")),
        title:"首页"
    },
    {
        path:"/index/menuCom",
        element:lazy(()=>import("../page/index/MenuCom")),
        title:"菜单"
    },
    {
        path:"/index/order",
        element:lazy(()=>import("../page/index/Order")),
        title:"订单"
    },
    {
        path:"/index/shopcar",
        element:lazy(()=>import("../page/index/ShopCar")),
        title:"购物车"
    }, 
    {
        path:"/index/my",
        element:lazy(()=>import("../page/index/My")),
        title:"我的"
    }
]

const routes = [
    {
        path:"*",
        element:lazy(()=>import("../page/Eorry404"))
    },
    {
        path:"/index",
        element:lazy(()=>import("../page/Index")),
        children:mainRouter
    },
    {
        path:"/login",
        element:lazy(()=>import("../page/Login"))
    },
    {
        path:"/details/:id",
        element:lazy(()=>import("../page/Details"))
    },
    {
        from:"/",
        to:"/index/home"
    }
]

export default routes