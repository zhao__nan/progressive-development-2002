import React ,{
  Suspense //弱网提示
} from 'react'
//引入封装好的路由表
import routes from './router_config'

import {
  BrowserRouter,// history 路由模式
  Routes,// 精确匹配每一个路由
  Route, // 路由试图
  Navigate // 重定向
} from "react-router-dom"

const SusLoding = () =>{
  return <>
    loding...
  </>
}

function index() {
  const renderRouter = (arr) => {
    return arr.map((item,index)=>{
      return item.path?<Route key={index} path={item.path} element={<item.element></item.element>}>
        {
          item.children&&renderRouter(item.children)
        }
      </Route>
      :<Route key={index} path={item.from} element={<Navigate to={item.to}/>}/>
    })
  }
  return (
    <Suspense fallback={<SusLoding/>}>
      <BrowserRouter>
        <Routes>
          {
            renderRouter(routes)
          }
        </Routes>
      </BrowserRouter>
    </Suspense>
  )
}

export default index