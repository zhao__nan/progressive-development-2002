import React from 'react'

// import {useNavigate} from "react-router-dom"
import withrouter from '../utils/withrouter';
function My_content({childrendata,router}) {
    // const navigate = useNavigate()
    console.log(router);
    const goto_details= (val) =>{
        console.log(val);
        // navigate("/details/"+val.id)
        //路由跳详情 传ID
        router.navigate(`/details/${val.id}`,{state:val})
    }
  return (
    <div>
        {
            childrendata&&childrendata.length>0?childrendata.map((val,i)=>{
            return <div key={i} onClick={()=>goto_details(val)}>
                <p>{val.title}</p>
                <img src={val.img} alt="图片错误" />
            </div>
            }):"没有数据"
        }
    </div>
  )
}

export default withrouter(My_content)