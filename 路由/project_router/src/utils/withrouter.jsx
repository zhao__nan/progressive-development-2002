import React from 'react'
/*
    withrouter
    useNavigate,// 跳路由的方法
    useLocation,// 获取路由传过来的参数
    useParams,// 获取地址栏id
*/
import {
    useNavigate,// 跳路由的方法
    useLocation,// 获取路由传过来的参数
    useParams,// 获取地址栏id
  } from "react-router-dom"
  
function withrouter(Com) {
  return (props) => {
    const params = useParams()
    const navigate = useNavigate()
    const location = useLocation()
    return <Com {...props} router = {{params,navigate,location}}></Com>
  }
}

export default withrouter