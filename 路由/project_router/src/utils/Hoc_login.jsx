import React from 'react'
import {Navigate} from "react-router-dom"

function Hoc_login(Com) {
  return (props) => {
    const user = window.localStorage.getItem("user")
    if (!user) {
       // 如果user 不存在 说明用户没有登陆 让用户去登陆页面
       return <Navigate to={"/login"}/>
    }
    return <Com {...props}></Com>
  }
}

export default Hoc_login

