import React from 'react'
// import {
//   useNavigate,// 跳路由的方法
//   useLocation,// 获取路由传过来的参数
//   useParams,// 获取地址栏id
// } from "react-router-dom"
import withrouter from '../utils/withrouter'

function Details({router}) {

  // const params = useParams()
  // const navigate = useNavigate()
  // const location = useLocation()
  // console.log(location);
  return (
    <div>
      Details
      <button onClick={()=>router.navigate("/index/home")}>点我</button>
      <p>地址拦id :{router.params.id}</p>
      <div >
          <p>{router.location.state.title}</p>
          <img src={router.location.state.img} alt="图片错误" />
      </div>
    </div>
  )
}

export default withrouter(Details) 