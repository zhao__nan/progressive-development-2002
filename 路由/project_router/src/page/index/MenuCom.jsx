import React ,{useState,useEffect} from 'react'
import {get_list} from "../../api/index"
import { Tabs } from 'antd';
import My_content from '../../components/My_content';
const { TabPane } = Tabs;
function MenuCom() {
  const [data,setData] = useState([])
  const [childrendata,setChildrenData] = useState([])
  useEffect(()=>{
    (async()=>{
      let res = await get_list()
      if (res.status === 200) {
        setData(res.data.list)
        setChildrenData(res.data.list[0].children)
      }
    })()
  },[])
  const onChange = (key) => {
    console.log(key);
    setChildrenData(data[key].children)
  };
  console.log(data);
  let my_content = {
    childrendata
  }
  return (
    <div className='menu'>
      <Tabs tabPosition={'left'} defaultActiveKey="1" onChange={onChange}>
          {
            data&&data.map((item,index)=>{
              return   <TabPane tab={item.title} key={index}>
              <My_content {...my_content}></My_content>
            </TabPane>
            })
          }
        
         
      </Tabs>
    </div>
  )
}

export default MenuCom