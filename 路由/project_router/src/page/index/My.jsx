import React from 'react'
// 引入封装好的路由守卫
import Hoc_login from '../../utils/Hoc_login'

function My() {
  return (
    <div>My</div>
  )
}

// 调用 路由守卫的方法
export default Hoc_login(My)