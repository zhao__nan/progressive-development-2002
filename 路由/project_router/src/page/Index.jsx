import React from 'react'
import {Outlet,NavLink} from "react-router-dom"
import {mainRouter} from "../router/router_config"

function Index() {
  return (
    <div className='index'>
      <header>头部</header>
      <main>
        <Outlet/>
      </main>
      <footer>
        {
          mainRouter.map((item,index)=>{
            return <NavLink to={item.path} key={index}>{item.title}</NavLink>
          })
        }
      </footer>
    </div>
  )
}

export default Index