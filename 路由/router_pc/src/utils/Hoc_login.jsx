import React from 'react'
import {Navigate} from 'react-router-dom'

function Hoc_login(Com) {
  return (props) => {
    const token = window.localStorage.getItem("token")
    if (!token) {
        return <Navigate to={"/login"}></Navigate>
    }
    return <Com {...props}></Com>
  }
}

export default Hoc_login