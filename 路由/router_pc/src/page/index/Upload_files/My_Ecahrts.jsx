import React,{useRef,useEffect} from 'react'
import * as echarts from "echarts"
import {option1,option2,option3} from "../../../utils/my_options"
function My_Ecahrts() {
  const line_chart = useRef()
  const histogram = useRef()
  const pie = useRef()
  useEffect(()=>{
    // 折线图
    const line_mychart = echarts.init(line_chart.current)
    line_mychart.setOption(option1)
    //柱状图
    const histogram_chart = echarts.init(histogram.current)
    histogram_chart.setOption(option2)
    //饼图
    const pie_chart = echarts.init(pie.current)
    pie_chart.setOption(option3)
    return () => {
        // 离开组建销毁
        line_mychart.dispose()
        histogram_chart.dispose()
        pie_chart.dispose()
    }
  },[])
  return (
    <div style={{display:"flex"}}>
      <div style={{width:"400px",height:"400px"}} ref={line_chart}></div>
      <div style={{width:"400px",height:"400px"}} ref={histogram}></div>
      <div style={{width:"400px",height:"400px"}} ref={pie}></div>
    </div>
  )
}

export default My_Ecahrts