import React from 'react'
import {Outlet,NavLink} from "react-router-dom"
import { FileOutlined, PieChartOutlined, UserOutlined ,DesktopOutlined,TeamOutlined} from '@ant-design/icons';
import { Breadcrumb, Layout, Menu } from 'antd';
import { useState } from 'react';

const { Header, Content, Footer, Sider } = Layout;

function Index() {
  const [collapsed, setCollapsed] = useState(false);
  const getItem = (label, key, icon, children) => {
    return {
      key,
      icon,
      children,
      label,
    };
  }
  const items = [
    getItem(<NavLink to={"/index/home"}>首页</NavLink> , '1', <PieChartOutlined />),
    getItem(<NavLink to={"/index/document"}>文本</NavLink> , '2', <DesktopOutlined />),
    getItem('组件', 'sub1', <UserOutlined />, [
      getItem(<NavLink to={"/index/component/project_m"}>project_m</NavLink> , '3'),
      getItem(<NavLink to={"/index/component/project_pc"}>project_pc</NavLink>, '4'),
    ]),
    getItem("上传文件", 'sub2', <TeamOutlined />, [
      getItem(<NavLink to={"/index/upload_files/echarts"}>echarts</NavLink>, '6'), 
      getItem(<NavLink to={"/index/upload_files/table"}>table</NavLink>, '8')
    ]),
    getItem(<NavLink to={"/index/command"}>指令</NavLink>, '9', <FileOutlined />),
  ];

  return (
    <div>
      <Layout
        style={{
          minHeight: '100vh',
        }}
      >
        <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} />
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
            }}
          />
          <Content
            style={{
              margin: '0 16px',
            }}
          >
          
            <div
              className="site-layout-background"
              style={{
                padding: 24,
                minHeight: 360,
              }}
            >
              <Outlet/>
            </div>
          </Content>
          <Footer
            style={{
              textAlign: 'center',
            }}
          >
            Ant Design ©2018 Created by Ant UED
          </Footer>
        </Layout>
      </Layout>
    </div>
  )
}

export default Index