import React ,{Suspense} from 'react'
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from "react-router-dom"
import routes from './router_config'

const SusLoding = () => {
  return <div>路由加载中...</div>
}
function index() {
  const renderRouter = (arr) => {
    return arr.map((item,index)=>{
      return item.path? <Route key={index} path={item.path} element={<item.element/>}>
        {
          item.children && renderRouter(item.children)
        }
      </Route>
      : <Route path={item.from} key={index} element={<Navigate to={item.to}/>}/>
    })
  }
  return (
    <Suspense fallback={<SusLoding></SusLoding>}>
      <BrowserRouter>
        <Routes>
          {
            renderRouter(routes)
          }
        </Routes>
      </BrowserRouter>
    </Suspense>
  )
}

export default index
