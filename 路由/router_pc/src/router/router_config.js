import { lazy } from "react";

const routes = [
    {
        path:"/index",
        element:lazy(()=>import("../page/Index")),
        children:[
            {
                path:"/index/home",
                element:lazy(()=>import("../page/index/Home"))
            },
            {
                path:"/index/document",
                element:lazy(()=>import("../page/index/Document"))
            },
            {
                path:"/index/component",
                element:lazy(()=>import("../page/index/component/Index")),
                children:[
                    {
                        path:"/index/component/project_m",
                        element:lazy(()=>import("../page/index/component/Project_m")),
                    },
                    {
                        path:"/index/component/project_pc",
                        element:lazy(()=>import("../page/index/component/Project_pc")),
                    }
                ]
            },
            {
                path:"/index/upload_files",
                element:lazy(()=>import("../page/index/Upload_files/Index")),
                children:[
                    {
                        path:"/index/upload_files/echarts",
                        element:lazy(()=>import("../page/index/Upload_files/My_Ecahrts")),
                    },
                    {
                        path:"/index/upload_files/table",
                        element:lazy(()=>import("../page/index/Upload_files/My_table")),
                    }
                ]
            },
            {
                path:"/index/command",
                element:lazy(()=>import("../page/index/Command"))
            }
        ]
    },
    {
        path:"/login",
        element:lazy(()=>import("../page/Login"))
    },
    {
        path:"*",
        element:lazy(()=>import("../page/Eorry404"))
    },
    {
        from:"/",
        to:"/index/home"
    }
]

export default routes

export let xxx
export let xxx1
export let xxx2