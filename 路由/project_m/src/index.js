import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './router/index';
import "./index.scss"
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a functio
