import React from 'react'
import {Image} from "antd-mobile"
import PropTypes from 'prop-types'
import withrouter from '../utils/withrouter'

function My_content({childrendata,router}) {
    const goto_detail = (item) => {
        router.navigate("/details/"+item.id,{state:item})
    }
  return (
    <div>
        {
            childrendata&&childrendata.length>0?childrendata.map((item,index)=>{
              return <div key={index} onClick={()=>goto_detail(item)}>
                <p>{item.name}</p>
                <Image width={100} src={item.img} lazy alt="1"/>
              </div>
            }):"没有数据"
          }
    </div>
  )
}

My_content.defaultProps = {
    childrendata: [{name:"没有传参数"}]
}

My_content.propTypes = {
    childrendata: PropTypes.array.isRequired
}
  
export default withrouter(My_content)