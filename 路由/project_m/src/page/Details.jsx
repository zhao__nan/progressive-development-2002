import React from 'react'
import withrouter from '../utils/withrouter'
import { NavBar,Image } from 'antd-mobile'
function Details({router}) {
    const back = () =>{
        router.navigate(-1)
    }
  return (
    <div>
         <NavBar onBack={back}>标题</NavBar>
         <div>
            <p>id:{router.params.id}</p>
            <p>{router.location.state.name}</p>
            <Image width={100} src={router.location.state.img} lazy alt="1"/>
        </div>
    </div>
  )
}

export default withrouter(Details)