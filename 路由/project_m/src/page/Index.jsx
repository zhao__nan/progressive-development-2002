import React from 'react'
import {Outlet,NavLink} from "react-router-dom"
import {  TabBar } from 'antd-mobile'
import {mainRouter} from "../router/router_config"
function Index() {
  return (
    <div className='index'>
      <header>头部</header>
      <main>
        <Outlet/>
      </main>
      <footer>
        <TabBar>
            {
              mainRouter.map((item,index) => (
                <TabBar.Item key={index} icon={item.icon} title={<NavLink to={item.path}>{item.title}</NavLink>} />
              ))
            }
        </TabBar>
      </footer>
    </div>
  )
}

export default Index