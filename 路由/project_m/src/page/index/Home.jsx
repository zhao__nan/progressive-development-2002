import React from 'react'
import { Swiper, Toast } from 'antd-mobile'
import styles from "./css/home.module.scss"
console.log(styles);
const colors = ['#ace0ff', '#bcffbd', '#e4fabd', '#ffcfac']

const items = colors.map((color, index) => (
  <Swiper.Item key={index}>
    <div
      className={styles.content}
      style={{ background: color }}
      onClick={() => {
        Toast.show(`你点击了卡片 ${index + 1}`)
      }}
    >
      {index + 1}
    </div>
  </Swiper.Item>
))
function Home() {
  return (
    <div className='home'>
        <Swiper autoplay>{items}</Swiper>
    </div>
  )
}

export default Home