import React ,{useEffect,useState} from 'react'
import * as api from "../../api/index"
import {SideBar} from "antd-mobile"
import My_content from '../../components/My_content'

function Classify() {
  const [activeKey,setActiveKey] = useState("0")
  const [data,setData] = useState([])
  const [childrendata,setChildrenData] = useState([])
  
  useEffect(()=>{
    (async()=>{
      let res = await api.get_list()
      if(res.status === 200){
        setData(res.data.list)
        setChildrenData(res.data.list[0].children)
      }
    })()
  },[])

  const change_Sidebar = (key) =>{
    console.log(key);
    setActiveKey(key)
    setChildrenData(data[key].children)
  }

  let my_content = {
    childrendata
  }
  return (
    <div className='classify'>
       <SideBar activeKey={activeKey} onChange={change_Sidebar}>
          {data&&data.map((item,index) => (
            <SideBar.Item key={index} title={item.title} />
          ))}
        </SideBar>
        <div className='content'>
         <My_content {...my_content}/>
        </div>
    </div>
  )
}

export default Classify