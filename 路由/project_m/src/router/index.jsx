import React,{Suspense} from 'react'
import routes from './router_config'
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate
} from "react-router-dom"

function index() {
  const renderRouter = (arr) => {
    return arr.map((item,index)=>{
      return item.path?<Route key={index} path={item.path} element={<item.element/>}>
        {
          item.children && renderRouter(item.children)
        }
      </Route>
      :<Route key={index} path={item.from} element={<Navigate to={item.to}/>}/>
    })
  }
  return (
    <Suspense fallback={<div>路由加载中</div>}>
      <Router>
        <Routes>
          {
            renderRouter(routes)
          }
        </Routes>
      </Router>
    </Suspense>
  )
}

export default index