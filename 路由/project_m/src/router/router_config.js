import { lazy } from "react";
import { UserOutline ,FrownOutline ,UnorderedListOutline ,ShopbagOutline,FolderOutline} from 'antd-mobile-icons'
export let mainRouter = [
    {
        path:"/index/home",
        element:lazy(()=>import("../page/index/Home")),
        title:"首页",
        icon:<ShopbagOutline />
    },
    {
        path:"/index/classify",
        element:lazy(()=>import("../page/index/Classify")),
        title:"分类",
        icon:<UnorderedListOutline />
    },
    {
        path:"/index/eat_food",
        element:lazy(()=>import("../page/index/Eat_food")),
        title:"吃什么",
        icon:<FrownOutline />
    },
    {
        path:"/index/shopcar",
        element:lazy(()=>import("../page/index/Shopcar")),
        title:"购物车",
        icon:<FolderOutline />
    }, 
    {
        path:"/index/my",
        element:lazy(()=>import("../page/index/My")),
        title:"我的",
        icon:<UserOutline />
    },
]

const routes = [
    {
        path:"*",
        element:lazy(()=>import("../page/Eorry404"))
    },
    {
        path:"/index",  
        element:lazy(()=>import("../page/Index")),
        children:mainRouter
    },
    {
        path:"/details/:id",
        element:lazy(()=>import("../page/Details"))
    },
    {
        path:"/login",
        element:lazy(()=>import("../page/Login"))
    },
    {
        from:"/",
        to:"/index/home"
    }
]

export default routes