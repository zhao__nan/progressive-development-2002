import React from 'react'
import {useLocation,useNavigate,useParams,useOutlet} from "react-router-dom"

function withrouter(Com) {
  return (props) => {
    const location = useLocation()
    const navigate = useNavigate()
    const params = useParams()
    const outlet = useOutlet()
    return <Com {...props} router={{location,params,navigate,outlet}}></Com>
  }
}

export default withrouter