import React ,{useEffect}from 'react'

import { Spin } from 'antd';

const indicator =(<div>加载中</div>)

function Wait_loding(props) {
  return (Com) => {
        debugger
    return  <Spin   
        delay={props.delay}
        indicator={indicator[props.loadingType] || props.indicator}//这里得type可以实现自定义loading,外面传进来 对应得type即可
        size={props.size}
        spinning={props.spinning || false}
        >

       <Com {...props}/>  
    </Spin>
  }
}
Wait_loding.defaultProps={
    size:'default',
    delay:1000,
    loadingType:'1',
    spinning:false
  };
export default Wait_loding

