import React from 'react'
import {Navigate} from "react-router-dom"

function Hoc_login(Com) {
  return (props) => {
    const token = window.localStorage.getItem("token")
    if (!token) {
        return <Navigate to={"/login"}/>
    }
    return <Com {...props}/>
  }
}

export default Hoc_login